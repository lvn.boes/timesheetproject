package be.variaNt.timeSheetApp.week;

import be.variaNt.timeSheetApp.day.WorkedDay;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class WorkedWeek {
    private String firstDayOfWeek;
    WorkedDay[] workedDays = new WorkedDay[7];


    public WorkedWeek (String firstDayOfWeek){
        setFirstDayOfWeek(firstDayOfWeek);
        workedDays = actualWorkedWeek(stringToLocalDate(firstDayOfWeek));
    }

    public WorkedDay[] getWorkedDays() {
        return workedDays;
    }

    public String getFirstDayOfWeek(){
        return firstDayOfWeek;
    }

    public void setFirstDayOfWeek(String firstDayOfWeek){
        this.firstDayOfWeek = firstDayOfWeek;
    }

    public static LocalDate stringToLocalDate(String firstDayOfWeek){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse(firstDayOfWeek, formatter);
    }

    public WorkedDay[] actualWorkedWeek(LocalDate firstDay){
        LocalDate day = firstDay;
        WorkedDay[] workWeek = new WorkedDay[7];
        for (int i = 0; i<7; i++){
            workWeek[i] = new WorkedDay(day);
            day = day.plusDays(1);
        }
        return workWeek;
    }

    public double totalBruto(){
        double totalBruto = 0;
        for (WorkedDay workday: workedDays) {
            totalBruto += workday.getBrutoPrice();
        }
        return totalBruto;
    }

    public double totalNetto (){
        double totalNetto = 0;
        for (WorkedDay workday: workedDays) {
            totalNetto += workday.getNettoPrice();
        }
        return totalNetto;
    }

    public void printWeekInfo(){
        System.out.println(this.toString());
        for (WorkedDay day:workedDays) {
            day.printDetailsWorkday();
        }
    }


    @Override
    public String toString() {
        return "Overview worked week: " +
                "\nBruto salary work week: " + String.format("%.2f", totalBruto()) +
                "\nNetto salary work week: " + String.format("%.2f", totalNetto()) +
                "\nVAT amount: " + String.format("%.2f", (totalBruto()-totalNetto())) + "\n";
    }
}
