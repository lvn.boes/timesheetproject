package be.variaNt.timeSheetApp.week;

public class WeekServiceImpl implements WeekService{
    private WorkedWeek workedWeek;

    @Override
    public WorkedWeek getWeek(){
        return workedWeek;
    }
    @Override
    public void createWeek(String firstDayOfWeek){
        this.workedWeek = new WorkedWeek(firstDayOfWeek);
    }
    @Override
    public void reset(String firstDayOfWeek){
        this.workedWeek = new WorkedWeek(firstDayOfWeek);
    }
}
