package be.variaNt.timeSheetApp.week;

public interface WeekService {
    void reset(String firstDayOfWeek);
    void createWeek(String firstDayOfWeek);

    WorkedWeek getWeek ();
}
