package be.variaNt.timeSheetApp.menu;

import be.variaNt.timeSheetApp.Keyboard;
import be.variaNt.timeSheetApp.slot.Slot;
import be.variaNt.timeSheetApp.week.WeekService;

public class MenuUtilities extends Menu{
    protected static boolean checkOverlap (Slot slot1, Slot slot2){
        boolean overlap = true;
        if (slot1.getEnd().isBefore(slot2.getStart()) || slot1.getEnd().equals(slot2.getStart())){
            overlap = false;
        } else if (slot1.getStart().isAfter(slot2.getEnd()) || slot1.getStart().equals(slot2.getEnd())){
            overlap = false;
        }
        return overlap;
    }

    protected static boolean checkSlotContained(Slot slot1, Slot slot2){
        boolean contained = false;
        if ((slot1.getStart().isAfter(slot2.getStart()) || slot1.getStart().equals(slot2.getStart()))
                && (slot1.getEnd().isBefore(slot2.getEnd())) || slot1.getEnd().equals(slot2.getEnd())) {
            contained = true;
        }
        return contained;
    }

    protected static int chooseWeekdayAndPrintNecessaryInfo(WeekService weekService, int detailsOrNot) {
        int dayChoice = -1;
        try {
            System.out.println("Choose a number between 1 and 7 to pick a day of the week starting on "
                    + weekService.getWeek().getFirstDayOfWeek());
            dayChoice = Keyboard.askForNumber() - 1;
            try {
                if (detailsOrNot == 0) {
                    weekService.getWeek().getWorkedDays()[dayChoice].printGeneralPaycheckWorkday();
                }
                else {
                    weekService.getWeek().getWorkedDays()[dayChoice].printDetailsWorkday();
                }
            } catch (IndexOutOfBoundsException iobe) {
                System.out.println("Invalid input. Choose a number between 1 and 7.");
                dayChoice = chooseWeekdayAndPrintNecessaryInfo(weekService, detailsOrNot);
            } catch (NullPointerException npe) {
                System.out.println("Error. You cannot print a paycheck before a week has been initialised");
                Keyboard.askForText();
            }
        } catch (NullPointerException npe) {
            System.out.println("Error. You cannot add or remove slots before a week has been initialised");
            Keyboard.askForText();
        }
        return dayChoice;
    }
}
