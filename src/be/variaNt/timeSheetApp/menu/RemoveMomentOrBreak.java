package be.variaNt.timeSheetApp.menu;

import be.variaNt.timeSheetApp.Keyboard;
import be.variaNt.timeSheetApp.day.WorkedDay;
import be.variaNt.timeSheetApp.slot.Slot;
import be.variaNt.timeSheetApp.week.*;


public class RemoveMomentOrBreak extends Menu{

    protected static void removeTimeSlotOrBreakSlot(WeekServiceImpl weekService) {
        System.out.println("Your choice: 4");
        int dayChoice = MenuUtilities.chooseWeekdayAndPrintNecessaryInfo(weekService, 1);
        if (dayChoice != -1) {
            if (weekService.getWeek().getWorkedDays()[dayChoice].getSlots().length == 0) {
                System.out.println("There are no slots to remove on this workday \n");
                Keyboard.askForText();
            } else {
                chooseAndRemoveSlot(weekService, dayChoice);
            }
        }
    }

    protected static void chooseAndRemoveSlot(WeekService weekService, int dayChoice) {
        System.out.println("Choose slot to remove");
        int slotChoice = Keyboard.askForNumber()-1;
        try {
            WorkedDay workedDay = weekService.getWeek().getWorkedDays()[dayChoice];
            Slot slotToRemove = workedDay.getSlots()[slotChoice];
            int count = 0;
            for (int i = 0 ; i < workedDay.getSlots().length ; i++) {
                if (!(MenuUtilities.checkSlotContained(workedDay.getSlots()[i], slotToRemove))) {
                    count++;
                }
            }
            Slot[] slotsToKeep = new Slot[count];
            int index = 0;
            for (int i = 0 ; i < workedDay.getSlots().length ; i++) {
                if (!(MenuUtilities.checkSlotContained(workedDay.getSlots()[i], slotToRemove))) {
                    slotsToKeep[index] = workedDay.getSlots()[i];
                    index++;
                }
            }
            weekService.getWeek().getWorkedDays()[dayChoice].setSlots(slotsToKeep);
            weekService.getWeek().getWorkedDays()[dayChoice].calculateMinutesByType();
            weekService.getWeek().getWorkedDays()[dayChoice].setBrutoPrice(weekService.getWeek().getWorkedDays()[dayChoice].calculateTotalPriceBruto(weekService.getWeek().getWorkedDays()[dayChoice].getMinutesByType()));
            weekService.getWeek().getWorkedDays()[dayChoice].setNettoPrice(weekService.getWeek().getWorkedDays()[dayChoice].nettoCalculator(weekService.getWeek().getWorkedDays()[dayChoice].getBrutoPrice()));
            System.out.println("You have removed slot " + (slotChoice + 1) + " and all break slots contained in it.\n");
        } catch (IndexOutOfBoundsException iobe) {
            System.out.println("Invalid input. Choose the number of an existing slot.");
            chooseAndRemoveSlot(weekService, dayChoice);
        }
    }
}
