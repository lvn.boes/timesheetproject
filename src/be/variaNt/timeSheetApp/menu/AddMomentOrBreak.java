package be.variaNt.timeSheetApp.menu;

import be.variaNt.timeSheetApp.Keyboard;
import be.variaNt.timeSheetApp.slot.*;
import be.variaNt.timeSheetApp.week.WeekService;
import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.InputMismatchException;

public class AddMomentOrBreak extends Menu {

    protected static void addWorkedMomentOrBreak(WeekService weekService) {
        System.out.println("Your choice: 3");
        int dayChoice = MenuUtilities.chooseWeekdayAndPrintNecessaryInfo(weekService, 1);
        if (dayChoice != -1) {
            System.out.println("Do you want to add a timeslot (1) or a breakslot (2)?\n" +
                    "Remember: You will have to add a night shift as two separate slots on the respective days."); //Choose timeslot or breakslot
            int choiceWorkOrBreak = Keyboard.askForNumber();
            while (choiceWorkOrBreak <= 0 || choiceWorkOrBreak >= 3) {
                System.out.println("Invalid input. Please enter 1 or 2.");
                choiceWorkOrBreak = Keyboard.askForNumber();
            }
            newSlot(choiceWorkOrBreak, weekService, dayChoice);
        }
    }

    protected static void newSlot (int choiceWorkBreak, WeekService weekService, int dayChoice){
        LocalTime start = enterStartTime();
        LocalTime end = enterEndTime(start);
        System.out.println("Enter a description for your slot"); //Enter description
        String description = Keyboard.askForText();
        Slot slot;
        if (choiceWorkBreak == 1) {
            slot = new TimeSlot(start, end, description);
            checkOverlapAndAddSlot(slot, weekService.getWeek().getWorkedDays()[dayChoice].getSlots(), choiceWorkBreak, weekService, dayChoice);
        } else {
            slot = new BreakSlot(start, end, description);
            checkOverlapAndAddSlot(slot, weekService.getWeek().getWorkedDays()[dayChoice].getSlots(), choiceWorkBreak, weekService, dayChoice);
        }
    }

    protected static LocalTime enterStartTime() {
        System.out.println("Enter start time (hh:mm)"); //Choose start time
        String startTime = Keyboard.askForText();
        while (startTime.equals("23:59")) {
            System.out.println("You cannot start a workday on the last minute of the day.");
            System.out.println("Enter start time (hh:mm)");
            startTime = Keyboard.askForText();
        }
        LocalTime start;
        try {
            start = Processor.stringToLocalTime(startTime);
        } catch (IndexOutOfBoundsException | NumberFormatException | DateTimeException ind) {
            System.out.println("Invalid input. Please enter a correct time using format hh:mm \n");
            start = enterStartTime();
        }
        return start;
    }

    protected static LocalTime enterEndTime(LocalTime start) throws InputMismatchException {
        System.out.println("Enter end time (hh:mm)"); //Choose end time
        String endTime = Keyboard.askForText();
        LocalTime end;
        try {
            end = Processor.stringToLocalTime(endTime);
        } catch (IndexOutOfBoundsException | NumberFormatException | DateTimeException ind) {
            System.out.println("Invalid input. Please enter a correct time using format hh:mm \n");
            end = enterEndTime(start);
        }
        try {
            if (end.isBefore(start)) {
                throw new InputMismatchException("Invalid input. Your end time cannot be before your start time.\n");
            }
        } catch (InputMismatchException ime) {
            System.out.println(ime.getMessage());
            end = enterEndTime(start);
        }
        return end;
    }

    protected static void checkOverlapAndAddSlot(Slot slot, Slot[] slots, int choiceWorkOrBreak, WeekService weekService, int dayChoice) {
        if (slot instanceof TimeSlot) {
            boolean overlap = false;
            for (Slot slotToCheck : slots) {
                overlap = MenuUtilities.checkOverlap(slotToCheck, slot);
            }
            if (overlap) {
                System.out.println("Your new slot overlaps another timeslot. Please enter another start and end time.");
                newSlot(choiceWorkOrBreak, weekService, dayChoice);
            } else {
                weekService.getWeek().getWorkedDays()[dayChoice].addSlot(slot);
                System.out.println("You have added a new timeslot from " + slot.getStart() + " to " + slot.getEnd() + "\n");
            }
        } else {
            if (slots.length == 0) {
                System.out.println("You cannot add a break without a preexisting timeslot.");
                addWorkedMomentOrBreak(weekService);
            } else {
                boolean contained = false;
                boolean overlap = false;
                for (Slot slotToCheck : slots) {
                    if ((MenuUtilities.checkSlotContained(slot, slotToCheck)) && (slotToCheck instanceof TimeSlot)) {
                        contained = true;
                    }
                    if ((MenuUtilities.checkOverlap(slot, slotToCheck)) && (slotToCheck instanceof BreakSlot)) {
                        overlap = true;
                    }
                }
                if (contained && !overlap) {
                    weekService.getWeek().getWorkedDays()[dayChoice].addSlot(slot);
                    System.out.println("You have added a new break slot from " + slot.getStart() + " to " + slot.getEnd() + "\n");
                } else if (contained && overlap) {
                    System.out.println("Your new slot overlaps another break slot. Please enter another start and end time.");
                    newSlot(choiceWorkOrBreak, weekService, dayChoice);
                } else if (!contained) {
                    System.out.println("Your break slot is not contained in an existing timeslot. Please enter another start and end time.");
                    newSlot(choiceWorkOrBreak, weekService, dayChoice);
                }
            }
        }
    }
}
