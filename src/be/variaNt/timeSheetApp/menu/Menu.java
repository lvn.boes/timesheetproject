package be.variaNt.timeSheetApp.menu;

import be.variaNt.timeSheetApp.Keyboard;
import be.variaNt.timeSheetApp.day.*;
import be.variaNt.timeSheetApp.week.*;
import java.time.*;

/**
 * This class creates a user menu and the necessary static methods to take and process user input and return the programs output.
 * This class is called from the main method and the mainMenu method runs until the program shuts down.
 * @author Francesca Van Coillie en Lieven Boes
 * @version 1.0
 */
public class Menu {

    public static void bootupMenu(){
        System.out.println("Welcome to our Timesheet App\nWe are booting up the system for you\n . . . Done\n+++++     =====     +++++");
        mainMenu();
    }

    public static void closedownMenu(){
        System.out.println("Thank you for using our app\nWe will close down everything and clean up for you.\n . . . Done\nSee you soon!");
    }

    public static void mainMenu(){
        WeekServiceImpl weekService = new WeekServiceImpl();
        boolean cont = true;
        while (cont) {
        System.out.println("+++++     =====     +++++\nWhat do you want to do?\nType in the number that corresponds with your choice\n" +
                "\n1. Show the different hourly rates" +
                "\n2. Start a new workweek" +
                "\n3. Add a worked moment or break" +
                "\n4. Remove a worked moment or break" +
                "\n5. Reset the current workweek" +
                "\n6. Print paycheck" +
                "\n7. Print detailed paycheck" +
                "\n0. Quit application" +
                "\n+++++     =====     +++++");

            int choice = Keyboard.askForNumber();
            if (choice >= 0 && choice < 8 ) {
                switch (choice) {
                    case 1:
                        showHourlyRates();
                        break;
                    case 2:
                        startNewWorkweek(weekService);
                        break;
                    case 3:
                        AddMomentOrBreak.addWorkedMomentOrBreak(weekService);
                        break;
                    case 4:
                        RemoveMomentOrBreak.removeTimeSlotOrBreakSlot(weekService);
                        break;
                    case 5:
                        ResetWeek.askForResetWeek(weekService);
                        break;
                    case 6:
                        System.out.println("Your choice: 6");
                        MenuUtilities.chooseWeekdayAndPrintNecessaryInfo(weekService, 0);
                        break;
                    case 7:
                        System.out.println("Your choice: 7");
                        try {
                            weekService.getWeek().printWeekInfo();
                        } catch (NullPointerException npe) {
                            System.out.println("Error. You cannot print a paycheck before a week has been initialised\n");
                        }
                        break;
                    case 0:
                        System.out.println("Your choice: 0");
                        closedownMenu();
                        cont = false;
                        break;
                }
            }
            else {
                    System.out.println("Invalid input \n");
                }
            }
        }

    private static void showHourlyRates() {
        System.out.println("Your choice: 1");
        System.out.println("Normal time weekday: " + Rates.WEEK.getNormalHourlyRate() +
                "\nOvertime weekday: " + Rates.WEEK.getOvertimeHourlyRate() +
                "\nSaturday: " + Rates.SAT.getNormalHourlyRate() +
                "\nSunday: " + Rates.SUN.getNormalHourlyRate());
    }

    private static void startNewWorkweek(WeekServiceImpl weekService) {
        System.out.println("Your choice: 2");
        if (weekService.getWeek() != null) {
            System.out.println("You have already initialised a week starting on " + weekService.getWeek().getFirstDayOfWeek() + ".\n");
        }
        else {
            System.out.println("Please enter a date (dd/mm/yyyy)");
            try {
                weekService.createWeek(Keyboard.askForText());
                System.out.println("You have created a new workweek starting on " + weekService.getWeek().getFirstDayOfWeek() + ".\n");
            } catch (DateTimeException | NumberFormatException | IndexOutOfBoundsException dnie) {
                System.out.println("Invalid input. Please enter a correct date using format dd/mm/yyyy \n");
                startNewWorkweek(weekService);
            }
        }
    }
}
