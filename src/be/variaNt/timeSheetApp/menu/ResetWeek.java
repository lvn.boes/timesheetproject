package be.variaNt.timeSheetApp.menu;

import be.variaNt.timeSheetApp.Keyboard;
import be.variaNt.timeSheetApp.week.WeekServiceImpl;
import java.time.DateTimeException;

public class ResetWeek extends Menu{
    protected static void askForResetWeek(WeekServiceImpl weekService) {
        System.out.println("Your choice: 5");
        try {
            System.out.println("Are you sure you want to reset the workweek starting from " + weekService.getWeek().getFirstDayOfWeek() + " (Y/N)?");
            String resetChoice = Keyboard.askForText().toUpperCase();
            while (!(resetChoice.equals("Y")) && !(resetChoice.equals("N"))) {
                System.out.println("Invalid input. Please enter 'Y' or 'N'.");
                resetChoice = Keyboard.askForText().toUpperCase();
            }
            if (resetChoice.equals("Y")) {
                resetWeek(weekService);
            } else {
                System.out.println("You have not reset your workweek\n");
            }
        } catch (NullPointerException npe) {
            System.out.println("You cannot reset a workweek that has not been initialised \n");
        }
    }

    protected static void resetWeek(WeekServiceImpl weekService) {
        System.out.println("Your week has been reset. Please enter a new start date (dd/mm/yyyy)");
        try {
            weekService.reset(Keyboard.askForText());
            System.out.println("You have created a new workweek starting on " + weekService.getWeek().getFirstDayOfWeek() + "\n");
        } catch (NumberFormatException | DateTimeException | IndexOutOfBoundsException ndie) {
            System.out.println("Invalid input. Please enter a correct date using format dd/mm/yyyy \n");
            resetWeek(weekService);
        }
    }
}
