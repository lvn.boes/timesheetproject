package be.variaNt.timeSheetApp;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class creates a Scanner and the necessary static methods to ask for user input via the keyboard.
 * This class can be used the following way:
 * <pre>
 *     Keyboard.askForText();
 *     keyboard.askForNumber();
 * </pre>
 * @author Francesca Van Coillie en Lieven Boes
 * @version 1.0
 * @see java.util.Scanner
 */
public class Keyboard {
    public static Scanner keyboard = new Scanner(System.in);

    /**
     * This method asks for user input from the keyboard and returns it as a string.
     * @return String of user input
     * @throws InputMismatchException
     */
    public static String askForText() {
        String text = "";
        try {
            text = keyboard.nextLine();
        } catch (InputMismatchException ime) {
            System.out.println("Invalid input. Please enter again.");
            keyboard = new Scanner(System.in);
            text = askForText();
        }
        return text;
    }

    /**
     * This method asks for numerical user input from the keyboard and returns it as an integer.
     * @return Integer value of user input
     * @throws InputMismatchException
     */
    public static int askForNumber() {
        int number = 0;
        try {
            number = keyboard.nextInt();
            keyboard.nextLine();
        } catch (InputMismatchException ime) {
            System.out.println("Invalid input. Please enter a number.");
            keyboard = new Scanner(System.in);
            number = askForNumber();
        }
        return number;
    }
}
