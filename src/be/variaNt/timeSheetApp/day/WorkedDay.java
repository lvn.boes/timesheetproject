package be.variaNt.timeSheetApp.day;

import be.variaNt.timeSheetApp.slot.*;
import java.time.*;

/**
 * This class is a workday with a date, an hourly rate that depends on the day of the week a fixed VAT-rate that is used to calculate the netto wage
 * and a number of time and break slots that can be added or removed by the user. It also contains methods to add new timeslots and calculate the
 * number of minutes normal time and over time and the bruto and netto price of both slots and days.
 * This class can be used in the following way
 * <pre>
 *     WorkedDay workedDay = new WorkedDay(LocalDate date);
 * </pre>
 * @author Francesca Van Coillie en Lieven Boes
 * @version 1.0
 * @see be.variaNt.timeSheetApp.slot.Slot
 */
public class WorkedDay implements Day {
    /**The time and break slots in a day*/
    private Slot[] slots = new Slot[0];
    private long[] minutesByType = new long[2];
    private LocalDate date;
    private DayOfWeek hourlyRate;
    private double brutoPrice;
    private double nettoPrice;
    public final double BTW = 0.79;



    public WorkedDay(LocalDate date){
        this.date = date;
        this.hourlyRate = date.getDayOfWeek();
    }


    public Slot[] getSlots() {
        return slots;
    }

    public double getBrutoPrice() {
        return brutoPrice;
    }

    public void setBrutoPrice(double brutoPrice) {
        this.brutoPrice = brutoPrice;
    }

    public double getNettoPrice() {
        return nettoPrice;
    }

    public void setNettoPrice(double nettoPrice) {
        this.nettoPrice = nettoPrice;
    }

    public long[] getMinutesByType() {
        return minutesByType;
    }

    public void setSlots(Slot[] slots) {
        this.slots = slots;
    }

    @Override
    public void addSlot(Slot slot) {
        Slot[] newTimeSlots = new Slot[slots.length + 1];
        System.arraycopy(slots, 0, newTimeSlots, 0, slots.length);
        newTimeSlots[slots.length] = slot;
        slots = newTimeSlots;
        calculateMinutesByType();
        this.brutoPrice = calculateTotalPriceBruto(minutesByType);
        this.nettoPrice = nettoCalculator(brutoPrice);
    }

    public void calculateMinutesByType() {
        long[] tempMinutesByType = new long[2];
        for (int i = 0; i < slots.length; i++) {
            if (slots[i] instanceof TimeSlot) {
                long[] workedMinutesByType;
                workedMinutesByType = Processor.slotToMinutesByType(slots[i].getStart(), slots[i].getEnd());
                tempMinutesByType[0] += workedMinutesByType[0];
                tempMinutesByType[1] += workedMinutesByType[1];
            } else {
                long[] breakMinutesByType;
                breakMinutesByType = Processor.slotToMinutesByType(slots[i].getStart(), slots[i].getEnd());
                tempMinutesByType[0] -= breakMinutesByType[0];
                tempMinutesByType[1] -= breakMinutesByType[1];
            }
        }
        minutesByType = tempMinutesByType;
    }

    public double calculateTotalPriceBruto(long[] minutesByType){
        return brutoCalculator(minutesByType)[0] + brutoCalculator(minutesByType)[1];
    }

    public double calculateNormalTimePriceBruto(long[] minutesByType){
        return brutoCalculator(minutesByType)[0];
    }
    public double calculateOverTimePriceBruto(long[] minutesByType){
        return brutoCalculator(minutesByType)[1];
    }

    public double[] brutoCalculator(long[] minutesByType){
        double priceNormalTimeMinutes = 0;
        double priceOverTimeMinutes = 0;
        Rates rate1 = Rates.WEEK;
        Rates rate2 = Rates.SAT;
        Rates rate3 = Rates.SUN;
        switch(hourlyRate.ordinal()){
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                priceNormalTimeMinutes = Processor.minutesToDecimalHours(minutesByType[0]) * rate1.getNormalHourlyRate();
                priceOverTimeMinutes = Processor.minutesToDecimalHours(minutesByType[1]) * rate1.getOvertimeHourlyRate();
                break;
            case 6:
                priceNormalTimeMinutes = Processor.minutesToDecimalHours(minutesByType[0]) * rate2.getNormalHourlyRate();
                priceOverTimeMinutes = Processor.minutesToDecimalHours(minutesByType[1]) * rate2.getOvertimeHourlyRate();
                break;
            case 7:
                priceNormalTimeMinutes = Processor.minutesToDecimalHours(minutesByType[0]) * rate3.getNormalHourlyRate();
                priceOverTimeMinutes = Processor.minutesToDecimalHours(minutesByType[1]) * rate3.getOvertimeHourlyRate();
                break;
        }
        return new double[]{priceNormalTimeMinutes, priceOverTimeMinutes};
    }

    public double nettoCalculator(double brutoPrice) {
        return brutoPrice * BTW;
    }

    public void printDetailsWorkday(){
        System.out.println(this.toString());
        int numberOfSlot = 1;
        for (Slot workSlots : slots){
            System.out.print("Slot " + numberOfSlot + ": \n");
            workSlots.printSlotInfo();
            if (workSlots instanceof TimeSlot) {
                System.out.println("Total price Time slot: " + String.format("%.2f", calculateTotalPriceBruto(workSlots.getMinutesByType())) + "\n");
                numberOfSlot++;
            } else {
                System.out.println("Total price Break slot: -" + String.format("%.2f", calculateTotalPriceBruto(workSlots.getMinutesByType())) + "\n");
                numberOfSlot++;
            }
        }
    }

    public void printGeneralPaycheckWorkday(){
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "**** " + date.getDayOfWeek() + " : " + date + " ****" +
                "\nHours normal time: " + String.format("%.2f", Processor.minutesToDecimalHours(minutesByType[0])) +
                "\nHours overtime: " + String.format("%.2f", Processor.minutesToDecimalHours(minutesByType[1])) +
                "\nTotal Bruto price normal time: " + String.format("%.2f", calculateNormalTimePriceBruto(minutesByType)) +
                "\nTotal Bruto price overtime: " + String.format("%.2f", calculateOverTimePriceBruto(minutesByType)) +
                "\nTotal Bruto price: " + String.format("%.2f", calculateTotalPriceBruto(minutesByType)) +
                "\nTotal Netto price: " + String.format("%.2f", nettoCalculator(calculateTotalPriceBruto(minutesByType))) + "\n";
    }
}

