package be.variaNt.timeSheetApp.day;

/**
 * This enumeration contains three categories of days (weekdays, saturdays, sundays) and has the normalTime and overTime hourly rates for each of these categories as instance variables.
 * These values can be accessed through their respective getters.
 * @author Francesca Van Coillie en Lieven Boes
 * @version 1.0
 */
public enum Rates {
    WEEK (15, 20),
    SAT (25,25),
    SUN (35,35);

    /**The normal hourly pay rate*/
    double normalHourlyRate;
    /**The hourly pay rate for overtime*/
    double overtimeHourlyRate;

    /**
     * This constructor creates a pay rate regime for a day
     * @param rateNormalTime The pay rate for normal time (between 08:00 and 18:00 including both limits)
     * @param rateOverTime The pay rate for over time (before 08:00 and after 18:00)
     */
    Rates (double rateNormalTime, double rateOverTime){
        this.normalHourlyRate = rateNormalTime;
        this.overtimeHourlyRate = rateOverTime;
    }

    /**
     * Getter for the instance variable normalHourlyRate
     * @return normalHourlyRate
     */
    public double getNormalHourlyRate() {
        return normalHourlyRate;
    }

    /**
     * Getter for the instance variable overtimeHourlyRate
     * @return overtimeHourlyRate
     */
    public double getOvertimeHourlyRate() {
        return overtimeHourlyRate;
    }
}
