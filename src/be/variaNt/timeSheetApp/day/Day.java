package be.variaNt.timeSheetApp.day;

import be.variaNt.timeSheetApp.slot.Slot;

/**
 * This interface implements the necessary methods for days in the timeSheetApp
 * @author Francesca Van Coillie en Lieven Boes
 * @version 1.0
 */
public interface Day {

    /**Forces classes that implement this interface to have the ability to add slots.*/
    void addSlot(Slot slot);
}
