package be.variaNt.timeSheetApp.slot;
/** This class is a break slot
 * @author Lieven Boes en Francesca Van Coillie
 * @version 1.0
 * */
import java.time.LocalTime;

public class BreakSlot implements Slot{
    private LocalTime start;
    private LocalTime end;
    private String description;
    private long totalMinutes;
    private long[] minutesByType;

    public BreakSlot (LocalTime start, LocalTime end, String description){
        this.start = start;
        this.end = end;
        this.description = description;
        this.totalMinutes = Processor.slotToDuration(start, end);
        this.minutesByType = Processor.slotToMinutesByType(start, end);
    }

    @Override
    public LocalTime getStart() {
        return start;
    }

    @Override
    public LocalTime getEnd() {
        return end;
    }


    @Override
    public long[] getMinutesByType() {
        return minutesByType;
    }

    /** Returns the printed information of the timeslot
     * */

    @Override
    public String toString() {
        return "**** Break slot info ****\nStart: " + start +
                "\nEnd: " + end +
                "\nDescription: " + description +
                "\nTotal number of hours: " + String.format("%.2f", Processor.minutesToDecimalHours(totalMinutes)) +
                "\nHours normal time: " + String.format("%.2f", Processor.minutesToDecimalHours(minutesByType[0])) +
                "\nHours overtime: " + String.format("%.2f", Processor.minutesToDecimalHours(minutesByType[1]));
    }

    /** Prints the information contained in the toString method
     */

    @Override
    public void printSlotInfo() {
        System.out.println(this.toString());
    }

}
