package be.variaNt.timeSheetApp.slot;

import java.time.LocalTime;

/** This interface provides the necessary attributes to create classes for different types of slots.
 * @author Lieven Boes en Francesca Van Coillie
 * @version 1.0
 * @see be.variaNt.timeSheetApp.slot
 * */

public interface Slot {
    LocalTime WORKDAY_START_HOUR = LocalTime.of(8,0);
    LocalTime WORKDAY_END_HOUR = LocalTime.of(18,0);

    LocalTime getStart ();
    LocalTime getEnd();
    long[] getMinutesByType();
    void printSlotInfo();

}
