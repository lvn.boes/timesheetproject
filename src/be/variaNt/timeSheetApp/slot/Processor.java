package be.variaNt.timeSheetApp.slot;

import java.time.*;
import java.time.format.DateTimeFormatter;

/** This class is the processor which contains different calculation methods
 * @author Lieven Boes en Francesca Van Coillie
 * @version 1.0
 * */

public class Processor {
    /**Returns the amount of minutes of the created timeslot or breakslot based on the entered start time and end time
     *
     * @param start The start time of the slot
     * @param end The end time of the slot
     * @return The total amounts of minutes of the specific slot
     */
    public static long slotToDuration(LocalTime start, LocalTime end){
        Duration duration = Duration.between(start, end);
        return duration.getSeconds()/60;
    }
    public static double minutesToDecimalHours (long numberOfMinutes){
        return (double)numberOfMinutes/60;
    }
    public static boolean isBeforeNormalStartWorkday(LocalTime time) {
        return time.isBefore(Slot.WORKDAY_START_HOUR);
    }
    public static boolean isAfterNormalEndWorkday(LocalTime time) {
        return time.isAfter(Slot.WORKDAY_END_HOUR);
    }
    public static long[] slotToMinutesByType(LocalTime start, LocalTime end) {
        long overtime = 0L;
        long normaltime = 0L;
        if (isBeforeNormalStartWorkday(start)){
            if (isBeforeNormalStartWorkday(end)){ //Begin en einde zijn beiden voor 8u
                overtime += slotToDuration(start, end);
            }
            else if (isAfterNormalEndWorkday(end)) { //Beginuur is voor 8u en einduur is na 18u
                overtime += slotToDuration(start, Slot.WORKDAY_START_HOUR);
                normaltime += slotToDuration(Slot.WORKDAY_START_HOUR, Slot.WORKDAY_END_HOUR);
                overtime += slotToDuration(Slot.WORKDAY_END_HOUR, end);
            }
            else { //Beginuur is voor 8u en einduur is tussen 8u en 18u
                overtime += slotToDuration(start, Slot.WORKDAY_START_HOUR);
                normaltime += slotToDuration(Slot.WORKDAY_START_HOUR, end);
            }
        }
        else if (isAfterNormalEndWorkday(start)){ //Beginuur en einduur zijn na 18u
            overtime += slotToDuration(start, end);
        }
        else {
            if (isAfterNormalEndWorkday(end)){ //Beginuur is tussen 8u en 18u en einduur is na 18u
                normaltime += slotToDuration(start, Slot.WORKDAY_END_HOUR);
                overtime += slotToDuration(Slot.WORKDAY_END_HOUR, end);
            }
            else { //Begin- en einduur zijn tussen 8u en 18u
                normaltime += slotToDuration(start, end);
            }
        }
        return new long[]{normaltime, overtime};
    }

    public static LocalTime stringToLocalTime(String timeString){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        return LocalTime.parse(timeString, formatter);
    }
}
